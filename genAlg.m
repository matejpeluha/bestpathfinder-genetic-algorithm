function genAlg(popSize, stateCords, numPoints)
%matica definujuca jedneho jedinca
MATRIX = newMatrix(numPoints);

%vytvorenie populacie
population = genrpop(popSize, MATRIX);

%vytvorenie preakazky
obstacles = createObstacle();

allBestLengths = [];
x = [];
%cykly/generacie jednotlivych populacii
for i = 1 : 2000

%GA pouzite na populaciu
population = oneCycleGenAlg(stateCords, population, MATRIX, popSize, obstacles);


bestWay = selbest(population, fitness(population, stateCords, obstacles), [1]);
bestWay = [ stateCords(1, : ), bestWay, stateCords(2, : ) ];
allBestLengths(i) = fitness(bestWay, stateCords, obstacles);
x(i) = i;
end

%skareda verzia vykreslovania
plotBestWay(bestWay, obstacles);

figure(2);
plot(x, allBestLengths);
end



