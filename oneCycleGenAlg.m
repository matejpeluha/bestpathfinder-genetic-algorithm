function newPop = oneCycleGenAlg(stateCords, pop, matrix, popSize, obstacles)
matrixNumBest = [2 1 1];
numBest = getNumOfBest(matrixNumBest);
numOfOthers = popSize - numBest;

y = fitness(pop, stateCords, obstacles);


%zaobstaranie minimalnej velkosti populacie
if popSize <= numBest
   fprintf("Prilis mala populacia");
   return;
end

%vyber najlepsich
bestPop = selbest(pop, y, matrixNumBest);

%vyber "ostatnych"
otherPop = selsort(pop, y, popSize);
otherPop = otherPop(1 : numOfOthers, : );

%mutacie a krizovanie "ostatnych"
otherPop = changeOtherPop(otherPop, matrix);

%zlucenie najlepsich a ostatnych
newPop = [bestPop; otherPop];
end