%tato funkcia funguje cisto na to ze ked zmenim maticu hovoriacu o tom
%kolko jedincov vyebram ako najlepsich, tak aby mi rovno prepocitalo pocet
%jedincov ktorych beriem do jedneho cislo ktore potrebujem neskor

function numBest = getNumOfBest(matrixNumBest)
%matrixNumBest je matica
%hovori ze kolko jedincov z danej pozicie(najlepsi, 2. atd.)berem

%zistenie poctu stlpcov v tejto matici
[~, columns] = size(matrixNumBest);

%pocet jedincov ktorych chcem vyberat ako najlepsich
numBest = 0;

%scitanie poctu jedincov ktorych chcem vyrbat ako najlepsich
for i = 1 : columns
numBest = numBest + matrixNumBest(i);
end
   
end