function dist = getDistance(indiv,statecords)
[~, columns] = size(indiv); % zistim velkost pola indiv (pocet bodov columns/2)
startPoint = (statecords(1, : )); % suradnice zaciatocneho bodu
endPoint = (statecords(2, : ));   % suradnice koncoveho bodu


dist = getDistancePoint(startPoint(1),startPoint(2),indiv(1),indiv(2)); % vzdialenost medzi zaciatocnym bodom a prvym bodom v na ceste



possitionInIndiv = 1;   % pozicia bodu od ktoreho sa pocita vzdialenost ku dalsiemu bodu
while ( columns - possitionInIndiv + 1) >= 4
    dist = dist + getDistancePoint(indiv(possitionInIndiv),indiv(possitionInIndiv+1),indiv(possitionInIndiv+2),indiv(possitionInIndiv+3));% vzdialenost bodov
    possitionInIndiv = possitionInIndiv + 2;
end
dist = dist + getDistancePoint(indiv(columns-1),indiv(columns),endPoint(1),endPoint(2));    % vzdialenost medzi predposlednym bodom a poslednym bodom v ceste

end