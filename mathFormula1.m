function result = mathFormula1(x1, y1, x2, y2, x3, y3, x4, y4)
up = (y3 - y4) * (x1 - x3) + (x4 - x3) * (y1 - y3);
down = (x4 - x3) * (y1 - y2) - (x1 - x2) * (y4 - y3);

result = up / down;
end