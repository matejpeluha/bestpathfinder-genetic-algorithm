clc;
%matica urcujuca velkosti hodnot v jednom riadku populacie(1 jedinec)
%1 jedinec = 1 cesta = 1 riadok

%velkost populacie
popSize = 50;
numPoints = 8;
startCord = [-450 -450];
endCord = [450 450];
STATE_CORDS = [startCord; endCord];

%funkcia genetickeho algoritmu
genAlg(popSize, STATE_CORDS, numPoints); 