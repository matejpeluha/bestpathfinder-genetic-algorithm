function [Fit] = fitness(Pop, stateCords, obstacles)
PENALTY = 50000;

[lpop, ~] = size(Pop);
Fit = [];


for i = 1 : lpop

   distance = getDistance(Pop(i, : ), stateCords); 
   
   if(isThereCollision(Pop(i, : ), stateCords, obstacles))
       distance = distance + PENALTY;
   end
   
   Fit(i) = distance;
end

end