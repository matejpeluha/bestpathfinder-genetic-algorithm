function bool = checkCollisions(x1, y1, x2, y2, obstacles)
[numObstacles, ~] = size(obstacles);
for i = 1 : numObstacles
    bool = checkCollisionsOneObstacle(x1, y1, x2, y2, obstacles(i, : ));
    if( bool == true )
        return;
    end
    
end

end


function bool = checkCollisionsOneObstacle(x1, y1, x2, y2, obstacle)
[~, obstaclesSize] = size(obstacle);
bool = false;

loop = 1;
while( loop <= obstaclesSize - 3)
    
    x3 = obstacle(loop);
    y3 = obstacle(loop + 1);
    x4 = obstacle(loop + 2);
    y4 = obstacle(loop + 3);
    
    result1 = mathFormula1(x1, y1, x2, y2, x3, y3, x4, y4);
    result2 = mathFormula2(x1, y1, x2, y2, x3, y3, x4, y4);
    
    if( (0 <= result1 && result1 <= 1) && (0 <= result2 && result2 <= 1))
    bool = true;
    return;
    end

    loop = loop + 2;
end


end