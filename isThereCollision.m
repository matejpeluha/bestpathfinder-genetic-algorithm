function bool = isThereCollision(individual, stateCords, obstacles)
[~, numPoints] = size(individual);
startCords = (stateCords(1, : ));
endCords = (stateCords(2, : ));

bool = checkCollisions(startCords(1), startCords(2), individual(1), individual(2), obstacles);
if( bool == true )
        return;
end

loop = 1;
while( loop <= numPoints - 3)
    x1 = individual(loop);
    y1 = individual(loop + 1);
    x2 = individual(loop + 2);
    y2 = individual(loop + 3);
    
    bool = checkCollisions(x1, y1, x2, y2, obstacles);
    
    if( bool == true)
        return;
    end
    
    loop = loop + 2;
end

bool = checkCollisions(individual(numPoints - 1), individual(numPoints), endCords(1), endCords(2), obstacles);

end