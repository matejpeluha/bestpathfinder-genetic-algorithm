function matrix = newMatrix(numPoints)
if numPoints < 1
   printf("Prilis malo bodov");
    return;
end

MAX_SIZE = 500;
MIN_SIZE = -500;
numVariables = 2 * numPoints;

matrix = [ MIN_SIZE * ones(1, numVariables); MAX_SIZE * ones(1, numVariables)];
end