%jednoduchy testovaci frontend - neupravena funkcia
function plotBestWay(bestWay, obstacles)
[~, Size] = size(bestWay);
Size = Size / 2;
bestWayX = [];
bestWayY = [];

i = 1;
loop = 1;
while (loop <= Size)
bestWayX(loop) = bestWay(i);
bestWayY(loop) = bestWay(i + 1);

loop = loop + 1;
i = i + 2;
end

figure(1);
plot(bestWayX, bestWayY, '-*');
hold on;

ob1X = [];
ob1Y = [];
ob2X = [];
ob2Y = [];
ob3X = [];
ob3Y = [];

[~, Size] = size(obstacles(1, : ));
Size = Size / 2;
i = 1;
loop = 1;
while (loop <= Size )
    
ob1X(loop) = obstacles(1, i);
ob1Y(loop) = obstacles(1, i + 1);

loop = loop + 1;
i = i + 2;
end


[~, Size] = size(obstacles(1, : ));
Size = Size / 2;

i = 1;
loop = 1;
while (loop <= Size )

ob2X(loop) = obstacles(2, i);
ob2Y(loop) = obstacles(2, i + 1);

loop = loop + 1;
i = i + 2;
end

[~, Size] = size(obstacles(1, : ));
Size = Size / 2;

i = 1;
loop = 1;
while (loop <= Size )

ob3X(loop) = obstacles(3, i);
ob3Y(loop) = obstacles(3, i + 1);

loop = loop + 1;
i = i + 2;
end



plot(ob1X, ob1Y);
hold on;
plot(ob2X, ob2Y);
hold on;
plot(ob3X, ob3Y);
hold on;

end
